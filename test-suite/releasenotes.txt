Release notes för testsviten. Dokumentet beskriver de ändringar som genomförts för varje release.
2.1 200511 Första versionen
2.1.1 200827 Uppdaterat med SjD mall
2.1.3 210318 Uppdaterat Testfall 7.3 och självdeklarationer
2.1.4 210428 Rättat mockar och lagt till testanvisningar och installationsbeskrivningar
2.1.5 220112 Uppdaterat stödlib till 1.8.0
2.1.6 230426 Anpassat självdeklarationerna till nya mallar